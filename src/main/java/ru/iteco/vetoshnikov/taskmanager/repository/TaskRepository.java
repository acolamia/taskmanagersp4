package ru.iteco.vetoshnikov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.iteco.vetoshnikov.taskmanager.model.Task;

import java.util.List;


@Repository
public interface TaskRepository extends JpaRepository<Task, String> {
    @Query(value = "SELECT a FROM Task a WHERE a.project.id=:projectId")
    List<Task> findAllByProjectId(@Param("projectId") @NotNull final String projectId);

    @Query(value = "SELECT a FROM Task a WHERE (a.id=:id AND a.project.id=:projectId)")
    Task getByProjectIdAndId(@Param("id") @NotNull final String id, @Param("projectId") @NotNull final String projectId);

    @Modifying
    @Query(value = "DELETE FROM Task a WHERE a.project.id=:projectId")
    void deleteAllByProjectId(@Param("projectId") @NotNull final String projectId);
}
