package ru.iteco.vetoshnikov.taskmanager.constant;

import java.util.HashMap;
import java.util.Map;

public enum StatusType {
    PLANNED("Запланировано"),
    INPROGRESS("В процессе"),
    COMPLETE("Выполнено");

    private final String displayName;

    StatusType(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    private static final Map<StatusType, String> VALUES;

    static {
        VALUES = new HashMap<>();
        for (StatusType someEnum : StatusType.values()) {
            VALUES.put(someEnum, someEnum.getDisplayName());
        }
    }

    public static Map<StatusType, String> getVALUES() {
        return VALUES;
    }
}
