package ru.iteco.vetoshnikov.taskmanager.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.iteco.vetoshnikov.taskmanager.api.ITaskService;
import ru.iteco.vetoshnikov.taskmanager.constant.StatusType;
import ru.iteco.vetoshnikov.taskmanager.dto.TaskDTO;

import javax.faces.bean.RequestScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RequestScoped
@Controller
public class TaskController {
    @Autowired
    private ITaskService taskService;

    private final Map<StatusType, String> statusTypeMap = StatusType.getVALUES();

    private List<TaskDTO> taskDTOList = new ArrayList<>();

    private TaskDTO tempTaskDTO;

    private String projectId;

    private String taskId;

    public void createEmptyTask() {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setProjectId(projectId);
        taskService.add(taskDTO);
    }

    public void delete(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        taskService.delete(id);
    }

    public String edit() {
        taskService.edit(tempTaskDTO);
        taskId = null;
        tempTaskDTO = null;
        return "taskListJsf?faces-redirect=true";
    }

    //GETTERS and SETTERS :

    public List<TaskDTO> getTaskDTOList() {
        return taskDTOList;
    }

    public void setTaskDTOList() {
        if (projectId == null || projectId.isEmpty()) taskDTOList = new ArrayList<>();
        else taskDTOList = taskService.allTasks(projectId);
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(@Nullable final String projectId) {
        this.projectId = projectId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(@Nullable final String taskId) {
        this.taskId = taskId;
    }

    public TaskDTO getTempTaskDTO() {
        return tempTaskDTO;
    }

    public void setTempTaskDTO() {
        if (taskId == null || taskId.isEmpty()) {
            tempTaskDTO = new TaskDTO();
            tempTaskDTO.setProjectId(projectId);
        } else tempTaskDTO = taskService.getById(taskId);
        taskId = null;
    }

    public Map<StatusType, String> getStatusTypeMap() {
        return statusTypeMap;
    }
    //GETTERS and SETTERS.
}
