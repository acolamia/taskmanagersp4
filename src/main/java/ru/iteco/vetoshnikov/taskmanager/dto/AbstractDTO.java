package ru.iteco.vetoshnikov.taskmanager.dto;

import java.util.UUID;

public class AbstractDTO {
    private String id= UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
