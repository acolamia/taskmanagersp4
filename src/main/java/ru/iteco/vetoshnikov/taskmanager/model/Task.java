package ru.iteco.vetoshnikov.taskmanager.model;

import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.iteco.vetoshnikov.taskmanager.constant.StatusType;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tasks")
public class Task extends AbstractModel {

    @ManyToOne
    @JoinColumn(name = "projectId")
    private Project project;

    @Column(name = "name")
    @Nullable
    private String name;

    @Column(name = "description")
    @Nullable
    private String description;

    @Column(name = "dateBegin")
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBegin;

    @Column(name = "dateEnd")
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateEnd;

    @Column(name = "statusType")
    @Enumerated(EnumType.STRING)
    private StatusType statusType = StatusType.PLANNED;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public StatusType getStatusType() {
        return statusType;
    }

    public void setStatusType(StatusType statusType) {
        this.statusType = statusType;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
